//
//  ViewController.m
//  MultiTableView
//
//  Created by Chandra@F22 on 07/09/15.
//  Copyright (c) 2015 F22Labs. All rights reserved.
//

#import "ViewController.h"
#import "View.h"

@interface ViewController ()

@property (nonatomic, strong) View *view;

@end

@implementation ViewController

@dynamic view;

- (void)loadView {
    [super loadView];
    
    self.view = [[View alloc] init];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
