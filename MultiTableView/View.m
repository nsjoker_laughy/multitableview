//
//  View.m
//  MultiTableView
//
//  Created by Chandra@F22 on 08/09/15.
//  Copyright (c) 2015 F22Labs. All rights reserved.
//

#import "View.h"
#import "MultiTable.h"

#define coloms 10

@interface View ()<MultiTableDelegate>

@property (nonatomic, strong) MultiTable *multiTableView;

@end

@implementation View

- (id)init {
    self = [super init];
    
    if (self) {
        [self createViews];
    }
    return self;
}

- (void)createViews {
    self.backgroundColor = [UIColor yellowColor];
    
    self.multiTableView = [[MultiTable alloc] initWithColoms:coloms];
    self.multiTableView.delegate = self;
    [self addSubview:self.multiTableView];
    
    [self.multiTableView registerTables];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.multiTableView.frame = CGRectMake(0, 50, self.frame.size.width, self.frame.size.height-50);
}

#pragma mark MultiTableDelegate Methods

- (CGFloat)getWidthForTableAtIndex:(NSInteger)index {
    CGFloat width;
    
    switch (index) {
        case 1: {
            width = 1000;
        }
        case 9: {
            width = 1000;
        }
            break;
        default:
            width = self.frame.size.width/coloms;
            break;
    }
    return width;
}

- (CGFloat)getWidthForScrollAtIndex:(NSInteger)index {
    return self.frame.size.width/coloms;
}

- (UIColor*)getBackgroundColorForScrollAtIndex:(NSInteger)index {
    UIColor *color = [UIColor whiteColor];
    return color;
}

- (UIColor*)getBackgroundColorForTableAtIndex:(NSInteger)index {
    UIColor *color = [UIColor redColor];
    return color;
}
- (void)registerTableView:(UITableView *)tableView {
    
    tableView.separatorInset = UIEdgeInsetsZero;
    tableView.layoutMargins = UIEdgeInsetsZero;
    
    NSString *reuseIdentifier = [NSString stringWithFormat:@"table%ld",tableView.tag-TABLETAG];
    
    [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:reuseIdentifier];
}

- (NSInteger)getNumberOfRowsForSection:(NSInteger)section {
    return NSIntegerMax-1;
}

- (UITableViewCell *)getCellForRowAtIndexPath:(NSIndexPath *)indexPath forTable:(UITableView*)tableView {
    
    UITableViewCell *cell = nil;
    NSString *reuseIdentifier = [NSString stringWithFormat:@"table%ld",tableView.tag-TABLETAG];
    
    switch (tableView.tag-TABLETAG) {
        case 0: {
            cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];
            
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
            }
            cell.backgroundColor = [UIColor greenColor];
        }
            break;
        case 1: {
            cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];
            
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
            }
            cell.backgroundColor = [UIColor blueColor];
        }
            break;
        case 2: {
            cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];
            
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
            }
            cell.backgroundColor = [UIColor grayColor];
        }
            break;
            
        default: {
            
            cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];
            
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
            }
            cell.backgroundColor = [UIColor orangeColor];
        }
            break;
    }
    
    cell.separatorInset = UIEdgeInsetsZero;
    cell.layoutMargins = UIEdgeInsetsZero;
    
    return cell;
}

- (CGFloat)getHeightForRowAtIndexPath:(NSIndexPath *)indexPath forTableWithIndex:(NSInteger)index {
    return 150;
}

- (void)multiTableViewDidSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

@end
