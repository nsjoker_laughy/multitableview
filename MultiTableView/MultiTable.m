//
//  MultiTable.m
//  MultiTableView
//
//  Created by Chandra@F22 on 07/09/15.
//  Copyright (c) 2015 F22Labs. All rights reserved.
//

#import "MultiTable.h"

@interface MultiTable ()<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>

@property (nonatomic) NSInteger numberOfColoms;

@end

@implementation MultiTable

- (id)initWithColoms:(NSInteger)coloms {
    
    self = [super init];
    
    self.numberOfColoms = coloms;
    
    if (self) {
        [self createViews];
    }
    return self;
}

- (void)createViews {
    self.backgroundColor = [UIColor orangeColor];
    
    for (NSInteger i=0; i<self.numberOfColoms; i++) {
        UIScrollView *scrollView = [[UIScrollView alloc] init];
        scrollView.tag = i+SCROLLTAG;
        scrollView.showsVerticalScrollIndicator = NO;
        scrollView.showsHorizontalScrollIndicator = NO;
        [self addSubview:scrollView];
        
        UITableView *tableView = [[UITableView alloc] init];
        tableView.tag = i+TABLETAG;
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.showsHorizontalScrollIndicator = NO;
        tableView.showsVerticalScrollIndicator = NO;
        [scrollView addSubview:tableView];
     }
}

- (void)registerTables {
    for (NSInteger i=0; i<self.numberOfColoms; i++) {
        UITableView *tableView = (UITableView*)[self viewWithTag:i+TABLETAG];
        [self.delegate registerTableView:tableView];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat x = 0;
    
    for (NSInteger i=0; i<self.numberOfColoms; i++) {
        
        UIScrollView *scrollView = (UIScrollView*)[self viewWithTag:i+SCROLLTAG];
        scrollView.frame = CGRectMake(x, 0, [self.delegate getWidthForScrollAtIndex:i], self.frame.size.height);
        x=x+[self.delegate getWidthForScrollAtIndex:i];
        scrollView.backgroundColor = [self.delegate getBackgroundColorForScrollAtIndex:i];
        
        UITableView *tableView = (UITableView*)[self viewWithTag:i+TABLETAG];
        tableView.frame = CGRectMake(0, 0, [self.delegate getWidthForTableAtIndex:i], self.frame.size.height);
        tableView.backgroundColor = [self.delegate getBackgroundColorForTableAtIndex:i];
        [scrollView setContentSize:CGSizeMake([self.delegate getWidthForTableAtIndex:i], self.frame.size.height)];
    }
}

#pragma mark TableView Delegate and DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.delegate getNumberOfRowsForSection:section];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self.delegate getCellForRowAtIndexPath:indexPath forTable:tableView];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self.delegate getHeightForRowAtIndexPath:indexPath forTableWithIndex:tableView.tag-TABLETAG];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    for (NSInteger i=0; i<self.numberOfColoms; i++) {
        UITableView *table = (UITableView*)[self viewWithTag:i+TABLETAG];
        
        [table selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
    
    [self.delegate multiTableViewDidSelectRowAtIndexPath:indexPath];
}

#pragma mark Scroll Delegate Methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (scrollView.tag <TABLETAG) {
        return;
    }
    
    CGPoint offset = scrollView.contentOffset;
    
    for (NSInteger i=0; i<self.numberOfColoms; i++) {
        UITableView *tableView = (UITableView*)[self viewWithTag:i+TABLETAG];
        [tableView setContentOffset:offset];
    }
}

@end
