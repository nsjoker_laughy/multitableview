//
//  MultiTable.h
//  MultiTableView
//
//  Created by Chandra@F22 on 07/09/15.
//  Copyright (c) 2015 F22Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SCROLLTAG 100
#define TABLETAG 200

@protocol MultiTableDelegate <NSObject>

- (UIColor*)getBackgroundColorForScrollAtIndex:(NSInteger)index;
- (CGFloat)getWidthForScrollAtIndex:(NSInteger)index;

- (void)registerTableView:(UITableView*)tableView;

- (CGFloat)getWidthForTableAtIndex:(NSInteger)index;
- (UIColor*)getBackgroundColorForTableAtIndex:(NSInteger)index;
- (NSInteger)getNumberOfRowsForSection:(NSInteger)section;
- (UITableViewCell*)getCellForRowAtIndexPath:(NSIndexPath*)indexPath forTable:(UITableView *)tableView;
- (CGFloat)getHeightForRowAtIndexPath:(NSIndexPath*)indexPath forTableWithIndex:(NSInteger)index;
- (void)multiTableViewDidSelectRowAtIndexPath:(NSIndexPath*)indexPath;

@end

@interface MultiTable : UIView

@property (nonatomic, weak) id <MultiTableDelegate> delegate;

- (id)initWithColoms:(NSInteger)coloms;
- (void)registerTables;

@end
