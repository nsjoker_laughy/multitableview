//
//  AppDelegate.h
//  MultiTableView
//
//  Created by Chandra@F22 on 07/09/15.
//  Copyright (c) 2015 F22Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

